HOSTNAME=`uname -n`
AMBIENTE=PROD
TEMPLATE=2017092501
IP=`ifconfig |grep '172.'|grep inet | awk '{ print $2 }'`
echo "###################################################"
echo "Host: $HOSTNAME"
echo "Ambiente: $AMBIENTE"
echo "Template: $TEMPLATE"
echo "Ip: $IP"
echo "###################################################"
